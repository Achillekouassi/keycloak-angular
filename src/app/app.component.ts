import { Component, OnInit } from '@angular/core';
import { KeycloakSecurityService } from './service/keycloak-security.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  public userName: string | undefined; // Propriété pour stocker le nom de l'utilisateur
  public email: string | undefined;

  constructor(public securityService: KeycloakSecurityService) {}

  ngOnInit(): void {
    this.userName = this.securityService.kc.tokenParsed?.['preferred_username'];
    this.email = this.securityService.kc.tokenParsed?.['email'];
  }

  onLogout(): void {
    this.securityService.kc.logout();
  }

  login(): void {
    this.securityService.kc.login();
  }

  // onChangePassword(): void {
  //   // Construire manuellement l'URL de la page de gestion du compte avec l'action "update-password"
  //   const accountManagementUrl = `${this.securityService.kc.createAccountUrl()}?action=update-password`;
  
  //   // Rediriger l'utilisateur vers l'URL
  //   window.location.href = accountManagementUrl;
  // }

  onChangePassword(): void {
    this.securityService.kc.accountManagement()
      .then(() => {
        console.log('Redirecting to change password page...');
      })
      .catch((error) => {
        console.error('Error while redirecting to change password page:', error);
      });
  }
  
  
}
