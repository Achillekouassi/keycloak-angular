import { Injectable } from '@angular/core';
import { KeycloakInstance } from 'keycloak-js';
declare var Keycloak: any;

@Injectable({
  providedIn: 'root'
})
export class KeycloakSecurityService {
  public kc: KeycloakInstance;

  constructor() {
    this.kc = new Keycloak({
      url: 'http://localhost:8080/',
      realm: 'reservation',
      clientId: 'angular-keycloak'
    });
  }

  async init() {
    console.log('Security initialization');
    try {
      await this.kc.init({
        onLoad: 'check-sso'
        // onLoad: 'login-required'
      });
      console.log('Keycloak initialized successfully');
      console.log(this.kc.token);
    } catch (error) {
      console.error('Error initializing Keycloak', error);
    }
  }
}
