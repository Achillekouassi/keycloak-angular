import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrl: './products.component.css'
})
export class ProductsComponent implements OnInit{
  products:any =[];

  constructor(){}
  ngOnInit(): void {
      this.products=[
        {id:1,name:"computer HP 54",price:7860},
        {id:2,name:"computer DELL 1554",price:5640},
        {id:3,name:"computer LENOVO 2254",price:3060},
        {id:4,name:"computer ACCER 154",price:1060},
        {id:5,name:"computer MACBOOK 254",price:9860},
      ]
  }
}
